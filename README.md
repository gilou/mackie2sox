mackie2sox allows to listen for Mackie Control Protocol transport (specific MIDI controls) from a Behringer X-Touch or anything else and control recording using 18 channels using sox. The first goal is to be able to use the X-Touch Mackie mode over USB to start the 18 channel recording, stop it, and play it back.

At startup the script will create a new folder with the date in YYYYMMDD-format as the name. A file index counter will be displayed on the timecode display, beginning with 1.

When you hit record, it will start recording the number of tracks (default= 18) in a new file that ends with 001. Hit STOP to stop recording and play to play the file. Hit record again and a new file with an incremented counter (002, 003, et cettera) will be created. REWIND and FAST FWD can be used to select multiple files and play them back.

Shutdown the pi by pressing MARKER and SOLO simultaneously.

# Dependencies

This requires sox (play/rec), Python 3.5+, and the psutil mido and rtmidi python modules, that usually can be obtained on Debian/Ubuntu using:

`sudo apt install sox python3-mido python3-rtmidi python3-psutil`

# Usage

./mackie2sox.py
