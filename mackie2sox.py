#! /usr/bin/env python3
"""
    mackie2sox - listen for MIDI/Mackie Control to record/playback/loop

    Copyright (C) 2020 Gilles Pietri <contact+dev@gilouweb.com>
    additions (C) 2020 René Knuvers <pythondev at reneknuvers dot nl>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from multiprocessing import Process
from subprocess import run

import mido
import psutil
import os
import datetime
import time
import os.path

DEFAULT_REC_FOLDER = "/media/pi/XR18/XR18"
DEFAULT_REC_PREFIX = "rec"
DEFAULT_REC_EXTENSION = "wav"
DEFAULT_MIDI_INPUT = "X-Touch MIDI 1"
DEFAULT_MIDI_OUTPUT = "X-Touch MIDI 1"

# Midi implementation of X-touch / Mackie Control Protocol
REC_CH1 = 0x00
REC_CH2 = 0x01
REC_CH3 = 0x02
REC_CH4 = 0x03
REC_CH5 = 0x04
REC_CH6 = 0x05
REC_CH7 = 0x06
REC_CH8 = 0x07

F1 = 0x36
F2 = 0x37
F3 = 0x38
F4 = 0x39
F5 = 0x3a
F6 = 0x3b
F7 = 0x3c
F8 = 0x3d

BANK_LEFT     = 0x2e
BANK_RIGHT    = 0x2f
CHANNEL_LEFT  = 0x30
CHANNEL_RIGHT = 0x31

MARKER = 0x54
NUDGE = 0x55
CYCLE = 0x56
DROP    = 0x57
REPLACE = 0x58
CLICK   = 0x59
SOLO    = 0x5a

REWIND = 0x5b
FAST_FWD = 0x5c
STOP     = 0x5d
PLAY     = 0x5e
RECORD   = 0x5f
TRANSPORT = range(MARKER, RECORD+1)

def show_timedisplay(text):
    text = str(text).rjust(12," ")
    for char_index in range(len(text)):
        xtouch_out.send(mido.Message("control_change", control=0x40 + (char_index % 12), value=ord(str(text[len(text)-1-char_index]))))

class ShutDownPi(Exception):
    pass

class RecordingManager:
    def __init__(self, filename="output.wav", midi_out=None):
        self.filename = filename
        self.rec_proc = None
        self.play_proc = None
        self.midi_out = midi_out

    def is_recording(self):
        return self.rec_proc and self.rec_proc.is_alive()

    def is_playing(self):
        return self.play_proc and self.play_proc.is_alive()

    def run_command(self, command, extra_params=[]):
        command.append(self.filename)
        command += extra_params

        def midi_message(command, midi_output="X-Touch MIDI 1"):
            if command[0] == "play":
                notes = [PLAY, CYCLE]
            else:
                notes = [RECORD]
            if self.midi_out:
                for note in notes:
                    self.midi_out.send(mido.Message("note_on", note=note, velocity=0))

        def run_in_process(command, callback):
            c = run(command, env=dict(AUDIODEV="hw:X18XR18,0"))
            midi_message(command)

        p = Process(target=run_in_process, args=(command, midi_message))
        p.start()
        return p

    def play(self):
        print("Play")
        if self.is_playing():
            print("Already playing")
        else:
            self.play_proc = self.run_command(["play"])
    def clear_buttons(self):
        '''Clear the lights on the buttons'''
        for note in RECORD, PLAY, CYCLE:
            self.midi_out.send(mido.Message("note_on", note=note, velocity=0))

    def process_cleanup(self, proc):
        if proc and proc.is_alive():
            for child in psutil.Process(proc.pid).children(recursive=True):
                if child.is_running():
                    child.terminate()
            proc.terminate()

    def stop(self):
        print("Stop")
        self.process_cleanup(self.play_proc)
        self.process_cleanup(self.rec_proc)
        self.play_proc = None
        self.rec_proc = None
        self.clear_buttons()

    def rec(self):
        if self.is_recording():
            print("Recording already in progress")
        else:
            print("Recording")
            self.rec_proc = self.run_command(["rec", "-c", "18"])

    def repeat(self):
        if self.is_playing():
            print("playing")
        else:
            print("Playing repeatedly")
            self.play_proc = self.run_command(["play"], ["repeat", "-"])
            with mido.open_output(DEFAULT_MIDI_OUTPUT) as midi_output:
                midi_output.send(mido.Message("note_on", note=CYCLE, velocity=127))
                midi_output.send(mido.Message("note_on", note=PLAY, velocity=127))

def cleanup():
    '''Close MIDI outputs'''
    try:
        if xtouch_out:
            xtouch_out.close()
        if xtouch_in:
            xtouch_in.close()
    except NameError:
        pass   

try:
    ShutDownCommandPrep = False
    file_idx_counter = 1
    datestamp = datetime.datetime.now().strftime("%Y%m%d")
    folder = DEFAULT_REC_FOLDER + "/" + datestamp
    try:
        os.mkdir(folder)
    except OSError:
        print("Warning: creation of folder {} failed ".format(folder))
    else:
        print("Succesfully created folder {} ".format(folder))

    file_name_format = "{}/{}_{:0>3d}.{}"
    file_name = file_name_format.format(folder, DEFAULT_REC_PREFIX, file_idx_counter, DEFAULT_REC_EXTENSION)

    xtouch_in = mido.open_input(DEFAULT_MIDI_INPUT)
    xtouch_out = mido.open_output(DEFAULT_MIDI_OUTPUT)
    rm = RecordingManager(midi_out=xtouch_out)

    # Clear the time display
    show_timedisplay("".center(12, " "))
    show_timedisplay(str(file_idx_counter))

    for msg in xtouch_in:
        inhibit = False
        if msg.type == "note_on" and msg.note in TRANSPORT:
            if msg.velocity == 127:
                if msg.note == STOP:
                    rm.stop()
                elif msg.note == PLAY:
                    rm.play()
                elif msg.note == RECORD:
                    while os.path.isfile(file_name):
                        file_idx_counter += 1
                        file_name = file_name_format.format(folder, DEFAULT_REC_PREFIX, file_idx_counter, DEFAULT_REC_EXTENSION)
                    show_timedisplay(file_idx_counter)
                    rm.filename = file_name
                    rm.rec()
                elif msg.note == CYCLE:
                    rm.repeat()
                elif msg.note == REWIND:
                    if file_idx_counter > 1:
                        file_idx_counter -= 1
                    file_name = file_name_format.format(folder, DEFAULT_REC_PREFIX, file_idx_counter, DEFAULT_REC_EXTENSION)
                    show_timedisplay(file_idx_counter)
                    rm.filename = file_name
                elif msg.note == FAST_FWD:
                    file_idx_counter += 1
                    file_name = file_name_format.format(folder, DEFAULT_REC_PREFIX, file_idx_counter, DEFAULT_REC_EXTENSION)
                    while not(os.path.isfile(file_name)) and file_idx_counter > 1:
                        file_idx_counter -= 1
                        file_name = file_name_format.format(folder, DEFAULT_REC_PREFIX, file_idx_counter, DEFAULT_REC_EXTENSION)
                    show_timedisplay(file_idx_counter)
                    rm.filename = file_name
                elif msg.note in (MARKER, SOLO) and not ShutDownCommandPrep:
                    ShutDownCommandPrep = True
                elif msg.note in (MARKER, SOLO) and ShutDownCommandPrep:
                    raise ShutDownPi('Shutdown command issued')
            elif msg.velocity == 0:
                if msg.note == RECORD and rm.is_recording():
                    inhibit = True
                elif msg.note in (PLAY, CYCLE) and rm.is_playing():
                    inhibit = True
                elif msg.note in (MARKER, SOLO):
                    ShutDownCommandPrep = False

        if not inhibit:
            xtouch_out.send(msg)
except ShutDownPi:
    cleanup()
    os.system("shutdown now")
except KeyboardInterrupt:
    print("Bye Bye")
finally:
    cleanup()
